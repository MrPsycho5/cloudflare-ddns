#!/usr/bin/python3
import requests
import os
import logging
import time

def get_public_ip():
    response = requests.get('https://api.ipify.org')
    return response.text

def check_token(token):
    url = "https://api.cloudflare.com/client/v4/user/tokens/verify"

    payload = {}
    headers = {
        "Content-Type": "application/json",
        "Authorization": "Bearer " + token
    }

    response = requests.request("GET", url, json=payload, headers=headers)

    if response.status_code == 200:
        return True
    return False


def get_record_id(domain, token, zone_identifier):
    url = "https://api.cloudflare.com/client/v4/zones/" + zone_identifier + "/dns_records"

    payload = {}
    headers = {
        "Content-Type": "application/json",
        "Authorization": "Bearer " + token
    }

    response = requests.request("GET", url, json=payload, headers=headers)

    for record in response.json()['result']:
        if record['name'] == domain:
            return record['id']
    return None

def get_record_value(token, zone_identifier, record_id):
    url = "https://api.cloudflare.com/client/v4/zones/" + zone_identifier + "/dns_records/" + record_id

    payload = {}
    headers = {
        "Content-Type": "application/json",
        "Authorization": "Bearer " + token
    }

    response = requests.request("GET", url, json=payload, headers=headers)

    return response.json()['result']['content']

def update_record(public_ip, domain, token, zone_identifier, record_identifier):
    url = "https://api.cloudflare.com/client/v4/zones/" + zone_identifier + "/dns_records/" + record_identifier

    payload = {
        "content": public_ip,
        "name": domain,
        "type": "A",
    }
    headers = {
        "Content-Type": "application/json",
        "Authorization": "Bearer " + token
    }

    response = requests.request("PUT", url, json=payload, headers=headers)

    if response.status_code == 200:
        return True, None
    else:
        return False, response.text
    
def main():
    try:
        token = os.environ['CLOUDFLARE_TOKEN']
        zone_identifier = os.environ['CLOUDFLARE_ZONE_IDENTIFIER']
        domain = os.environ['DOMAIN']
    except KeyError:
        logging.error("Please set CLOUDFLARE_TOKEN, CLOUDFLARE_ZONE_IDENTIFIER and DOMAIN environment variables")
        exit(1)

    is_token_valid = check_token(token)
    if not is_token_valid:
        logging.error("Token is not valid")
        exit(1)

    record_id = get_record_id(domain, token, zone_identifier)
    if record_id is None:
        logging.error("Record matching " + domain + " not found. Please create it first in Cloudflare console")
        return

    public_ip = get_public_ip()
    if public_ip == "":
        logging.error("Unable to obtain public IP. Is access to https://api.ipify.org allowed?")
        return

    current_record = get_record_value(token, zone_identifier, record_id)
    logging.debug("Current public IP: " + public_ip)
    logging.debug("Current record value: " + current_record)

    if public_ip == current_record:
        logging.info("Public IP is the same as record value. No update needed")
    else:
        logging.warning("Public IP is different than record value. Updating record")
        (result, error) = update_record(public_ip, domain, token, zone_identifier, record_id)
        if result:
            logging.info("Record updated successfully")
        else:
            logging.error("Error updating record: " + error)
            return



if __name__ == '__main__':
    try:
        interval = int(os.environ['INTERVAL'])
    except KeyError:
        interval = 300

    logging.basicConfig(level=logging.INFO, format='%(asctime)s %(levelname)s %(message)s')

    while True:
        main()
        time.sleep(interval)
