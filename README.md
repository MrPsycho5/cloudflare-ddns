## Usage:

1. Create a Cloudflare API Token with the following permissions:
    - Zone / DNS / Edit
    - Include / Specific Zone / Your zone
    
    Documentation [here](https://developers.cloudflare.com/fundamentals/api/get-started/create-token/).

2. Obtain your Zone ID from the Overview tab of your domain in the Cloudflare dashboard. Documentation [here](https://developers.cloudflare.com/fundamentals/setup/find-account-and-zone-ids/).
3. Use `docker run` or `docker compose up` (preferred) to start the container. Make sure to set correct environment variables.

Your domain will be updated every 5 minutes by default. You can change this by setting the `INTERVAL` environment variable.

```bash
docker run -e CLOUDFLARE_TOKEN=value -e CLOUDFLARE_ZONE_IDENTIFIER=value -e DOMAIN=sub.domain.tld -e INTERVAL=300 registry.gitlab.com/mrpsycho5/cloudflare-ddns:latest
```

```docker-compose
version: '3.7'
services:
  cloudflare-ddns:
    image: registry.gitlab.com/mrpsycho5/cloudflare-ddns:latest
    container_name: cloudflare-ddns
    restart: unless-stopped
    environment:
      - CLOUDFLARE_TOKEN=value
      - CLOUDFLARE_ZONE_IDENTIFIER=value
      - DOMAIN=sub.domain.tld
      - INTERVAL=300
```

```bash
docker compose up -d
```

## Updating

```bash
docker pull registry.gitlab.com/mrpsycho5/cloudflare-ddns:latest
docker compose up -d
```

## Development

Please create virtual environment and install requirements.txt

```bash
python3 -m virtualenv .venv
source .venv/bin/activate
pip install -r requirements.txt
```